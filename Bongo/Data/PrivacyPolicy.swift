//
//  PrivacyPolicy.swift
//  Bongo
//
//  Created by Macbook Pro on 25/7/20.
//  Copyright © 2020 kamrulhassansabuj. All rights reserved.
//
import Foundation

// MARK: - PrivacyPolicy
struct PrivacyPolicy: Codable {
    let embedded: Embedded

    enum CodingKeys: String, CodingKey {
        case embedded = "_embedded"
    }
}

// MARK: - Embedded
struct Embedded: Codable {
    let id: Int
    let title, systemTitle, embeddedDescription: String

    enum CodingKeys: String, CodingKey {
        case id, title
        case systemTitle = "system_title"
        case embeddedDescription = "description"
    }
}
