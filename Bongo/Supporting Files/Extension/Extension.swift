//
//  Extension.swift
//  Bongo
//
//  Created by Macbook Pro on 25/7/20.
//  Copyright © 2020 kamrulhassansabuj. All rights reserved.
//

import Foundation
import UIKit
//MARK: - Data to string
extension Data{
    func converToString() -> String{
        return String(decoding: self, as: UTF8.self)
    }
}
//Button Loading indicator
extension UIButton {
    func loadingIndicator(_ show: Bool) {
        let tag = 808404
        if show {
            self.isEnabled = false
            self.alpha = 0.5
            let indicator = UIActivityIndicatorView()
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
            indicator.tag = tag
            self.addSubview(indicator)
            indicator.startAnimating()
        } else {
            self.isEnabled = true
            self.alpha = 1.0
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
}
