//
//  APICall.swift
//  Bongo
//
//  Created by Macbook Pro on 25/7/20.
//  Copyright © 2020 kamrulhassansabuj. All rights reserved.
//

import Foundation
//MARK : - Fetching basic data
func fetchData(authorizationTocken : String, url : String , completion: @escaping (_ response: Data) -> Void) {
    let jsonURLString = url
    print(jsonURLString)
    guard let url = URL(string: jsonURLString) else {
        return
    }
    var request = URLRequest(url: url)
    request.httpMethod = "GET"
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    request.addValue(authorizationTocken, forHTTPHeaderField: "Authorization")
//    request.httpBody = newBody.percentEscaped().data(using: .utf8)
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard let data = data else {return}
        completion(data)
        }.resume()
}
