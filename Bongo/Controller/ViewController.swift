//
//  ViewController.swift
//  Bongo
//
//  Created by Macbook Pro on 25/7/20.
//  Copyright © 2020 kamrulhassansabuj. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let authToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1VTRVJfQklPU0NPUEUiLCJST0xFX1VTRVIiLCJST0xFX1VTRVJfQU5PTllNT1VTIl0sInVzZXJuYW1lIjoiYm9uZ29fYW5vbnltb3VzIiwiY2xpZW50X2xvZ2luX2lkIjo1NjcyNDg0MjUwNSwiYm9uZ29faWQiOiJrekNNVEdJd3lFYSIsImlhdCI6MTU5NTYyMzc3OSwiZXhwIjoxNTk4MjE1Nzg5LjB9.usx8nsMu-Pg54NBYAe_I3_EkJop-pLnDAW_ZqGpV6hxTVZNlcV8n3CM3MnFkRLNFAlGKC6rFscqF6MR35PwI9Q33kgsBNyXwRK7yTEGqG3xQegP0sZWFzWPkBr2sZLWkfdIXL-jixD5K5r0LIJG1jCqn4zyJN1cEQxo-HC6z-SxuQatTAC641zAxyrE-F0MYEKcdc91ZAyU01bMYZjQ7SkEPDCv_diV_QLYuAcq6kXKwIOA7hb1iOz02OrbDv0TdS70lVdvhuMfpcm1Cco7bvugAsgS2Iv5k5jhhjE4W4WxxMRUWBGV3iI7JTEozUE-kEgKuwGu06m9fWQSvV7oGOSVNsrA4-9f-Reou3r9GUk25q-wPAsKnYh6NE4BX43Du-AVjPNg4KKDNhJ7_VERhL2i84F_gWr0_Cg3VpDse8iDOwSoS7q2nuY30nxLcRHyNkLcANHtXwOqH2-8TmB_PQd5vokHVn8GaKXCcVMGImXoDvHAueVc2Tr4JUK_aEhAcPbhdtZKZjIBWY9-4udX8R57IDqc2BkXG_87eI7eQjm2HinQUoBEmz_pNcpnz0nvYELFNwoaLEXui9Mvrv-RzwCekOPPorvnVkkcg3uVdbf2Qw7sDVwCcq_7LKWwh47loKEz7pGriSXsF3JBoQlvHOk1d3Nzltb5UcVWLbu457xo"
    //IBOutlets
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var apiCallButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //apply corner radious to api call button
        apiCallButton.layer.cornerRadius = apiCallButton.layer.frame.height / 2
    }
//MARK: - API call button pressed action
    @IBAction func apiCallButtonPressed(_ sender: UIButton) {
        textView.text = ""
        apiCallButton.loadingIndicator(true)
        fetchData(authorizationTocken: authToken, url: Links.privacy) { (responseData) in
            do {
                let data = responseData
                let results = try JSONDecoder().decode(PrivacyPolicy.self, from: data)
                print("result in String : \(data.converToString())")
                let rawDescription = results.embedded.embeddedDescription.decodingHTMLEntities()
                print("result : \(results.embedded.embeddedDescription.decodingHTMLEntities())")
                print("result last character: \(results.embedded.embeddedDescription.last)")
                var tenthCharacterArray = [String]()
                var everyWordCountArray = [String]()
                let words = rawDescription.components(separatedBy: " ")
                print("words : \(words)")
                for (index, character) in rawDescription.enumerated(){
                    if index.isMultiple(of: 10){
                        tenthCharacterArray.append("\(character)")
                    }
                    
                }
                for word in words.enumerated(){
                    everyWordCountArray.append("\(word.element.count)")
                    
                }
                DispatchQueue.main.async {
                    print("array : \(tenthCharacterArray.joined(separator:","))")
                    self.textView.text = "Last character : \(results.embedded.embeddedDescription.decodingHTMLEntities().last)\n\nEvery 10th character : \n\(tenthCharacterArray.joined(separator:", "))\n\nCount of every word : \n\(everyWordCountArray.joined(separator:", "))"
                    self.apiCallButton.loadingIndicator(false)
                }
            } catch {
                print("error: \(error.localizedDescription)")
            }
        }
    }
    
}

